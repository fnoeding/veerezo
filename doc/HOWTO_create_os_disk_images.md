
How to create an os disk image
==============================

In this example an Ubuntu 11.10 server will be installed.


Initial Preparations
--------------------

        # Create a sparse image file:
        dd if=/dev/zero of=ubuntu-11.10-server.raw bs=1G seek=4 count=0

        # Start kvm using this image file
        kvm -hda=ubuntu-11.10-server.raw -cdrom=... ...


Install the OS
--------------

Install Ubuntu server as usual but do not boot into the system after the installation finished. Just shutdown kvm:

* use "C" locale,
* use timezone UTC,
* use the whole disk with a single partition without LVM, mount the filesystem with "noatime",
* format the single partition with ext4fs or your other favorite FS,
* install openssh,
* after the installation finished shutdown the virtual machine.


Prepare the OS for Veerezo
--------------------------

        # prepare for mounting the system partition
        losetup $LOOPDEVICE ubuntu-11.10-server.raw
        kpartx -pp -a $LOOPDEVICE

        # configure fsck using tune2fs to never run
        tune2fs -i 0 -c 0 ${LOOPDEVICE}p1

        # mount the system partition to your working area
        mount ${LOOPDEVICE}p1 $MOUNTPOINT

        # delete the normal user added during installation
        chroot $MOUNTPOINT userdel $USERNAME_DURING_INSTALLATION

        # lock the password based login of root (logins using ssh are still possible)
        chroot $MOUNTPOINT passwd -l root

        # edit /etc/fstab
        #     /dev/sda1 -> /
        #     /dev/sdb1 -> swap
        #     /dev/sdc1 -> data
        ...

        # edit /etc/apt/sources
        ...

        # update / upgrade (you probably have to edit $MOUNTPOINT/etc/resolv.conf)
        chroot $MOUNTPOINT apt-get update
        chroot $MOUNTPOINT apt-get upgrade

        # install acpid (allows the vm to safely shutdown instead of getting killed with kill -9
        chroot $MOUNTPOINT apt-get install acpid

        # configure openssh:
        #     - disable password based logins, allow only pubkey auth (PasswordAuthentication no, ChallengeResponseAuthentication no).
        #     - disable protocol version 1
        #     - configure the host filenames: /etc/ssh/ssh_host_rsa_key and /etc/ssh/ssh_host_dsa_key
        ...

        # remove openssh host keys
        ...

        # remove any authorized keys from the root user
        ...

        # remove udev rules related to network cards: the MACs will be wrong

        # trim down the installation a bit more
        ...

        # remove any history files or other unwanted files
        ...

        # umount and cleanup using kpartx, losetup.
        umount $MOUNTPOINT
        kpartx -pp -d $LOOPDEVICE
        losetup -d $LOOPDEVICE

Done!
-----

Done! At least if you never had any security relevant data in the image file. If there is any security related data you have to manually recreate the disk layout and rsync all files to the clean image. If you did nothing but a plain install and created the user with trivial credentials everything should be fine. This recreation can also reduce the image size.
