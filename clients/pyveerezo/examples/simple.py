#!/usr/bin/python

import pyveerezo


veerezo = pyveerezo.VeerezoClient('http://127.0.0.1:8080', 'user', 'pass')

tagForThisRun = 'simple'

netcfg = pyveerezo.VMNetworkConfigMaker()
netcfg.setDNS(['8.8.8.8', '8.8.4.4'])

try:
    vmConfig = {
        'ramMiB': 256,
        'image': '/images/3',
        'networkCards': ['/networks/1'],
        'networkConfiguration': netcfg.setIPv4Static('192.168.42.100', '255.255.255.0', '192.168.42.1').get(),
        'tags': [tagForThisRun],
    }

    vm = veerezo.addVM(**vmConfig)
    veerezo.startVM(vm)
    veerezo.waitForVMState(vm, pyveerezo.States.Running, 60.0)

    raw_input('VM is running! Do something with it and when you are done hit [enter]...')
finally:
    for vm in veerezo.getVMs([tagForThisRun]):
        veerezo.deleteVM(vm)

    for net in veerezo.getNetworks([tagForThisRun]):
        veerezo.deleteNetwork(net)

