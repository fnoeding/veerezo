#!/usr/bin/python

import pyveerezo



def main():
    #    E 1<---net4---\
    #                  |
    #                  v
    #                  3
    # A 1<---net1--->1 B 2<---net2--->1 C 2<---net3--->1 D
    #
    # all hosts are also connected to netConfig


    # first we need to create a client instance
    veerezo = pyveerezo.VeerezoClient('http://127.0.0.1:8080', 'user', 'pass')

    # we will later use this network to access the virtual machines
    # TODO at the moment the veerezo code does not support shared / global / public networks. This network is hard coded so far.
    netConfig = '/networks/1'

    # use a Ubuntu 11.10 server image
    image = '/images/3'

    # pick a name that is unique for your user. This helps us cleanup later
    tagForThisRun = 'advanced'


    try:
        nets = []
        vms = []

        # create the networks
        for i in range(4):
            net = veerezo.addNetwork([], # no host system devices attached
                                     [tagForThisRun, str(i + 1)]) # tags
            nets.append(net)


        # creating a network configuration is simpler with this helper
        netcfg = pyveerezo.VMNetworkConfigMaker()
        netcfg.setDNS(['8.8.8.8', '8.8.4.4']) # use the google DNS servers

        # create the configuration for the VMs
        vmConfigs = [
            {'tags': [tagForThisRun, 'A'],
             'ramMiB': 256,
             'image': image,
             'networkCards': [netConfig, nets[0]], # a network card must either be attached to no device (None) or a valid network
             'networkConfiguration': netcfg.setIPv4Static('192.168.42.101', '255.255.255.0').get(),
            },
            {'tags': [tagForThisRun, 'B'],
             'ramMiB': 256,
             'image': image,
             'networkCards': [netConfig, nets[0], nets[1], nets[3]],
             'networkConfiguration': netcfg.setIPv4Static('192.168.42.102', '255.255.255.0').get(),
            },
            {'tags': [tagForThisRun, 'C'],
             'ramMiB': 256,
             'image': image,
             'networkCards': [netConfig, nets[1], nets[2]],
             'networkConfiguration': netcfg.setIPv4Static('192.168.42.103', '255.255.255.0').get(),
            },
            {'tags': [tagForThisRun, 'D'],
             'ramMiB': 256,
             'image': image,
             'networkCards': [netConfig, nets[2]],
             'networkConfiguration': netcfg.setIPv4Static('192.168.42.104', '255.255.255.0').get(),
            },
            {'tags': [tagForThisRun, 'E'],
             'ramMiB': 256,
             'image': image,
             'networkCards': [netConfig, nets[3]],
             'networkConfiguration': netcfg.setIPv4Static('192.168.42.105', '255.255.255.0').get(),
            },
        ]

        # create the VMs
        for vmConfig in vmConfigs:
            vm = veerezo.addVM(**vmConfig)
            vms.append(vm)


        # show the networks we have
        print veerezo.getNetworks()

        # show the VMs we have
        print veerezo.getVMs()

        # start the VMs. This is NOT done automatically for you!
        for vm in vms:
            veerezo.startVM(vm)

        # wait for the VMs to become ready
        print 'Waiting for VMS to become ready'
        for vm in vms:
            veerezo.waitForVMState(vm, pyveerezo.States.Running, 60.0)
            print '    {0} is ready'.format(vm)


        # end of demo
        raw_input('VMs are ready. You can stop them again by pressing [enter].')
    finally:
        # delete VMs
        for vm in veerezo.getVMs([tagForThisRun]):
            veerezo.deleteVM(vm)

        # delete networks
        for net in veerezo.getNetworks([tagForThisRun]):
            veerezo.deleteNetwork(net)



if __name__ == '__main__':
    main()
