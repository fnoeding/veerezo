import requests
import json
import time
import copy
import urllib

# XXX known issues
# python-requests 0.5.0-1 confuses auth data if multiple veerezo clients with different user auth data are used in the same python process.


# XXX ubuntu only has python-requests 0.5.0-1
# in a newer version we could use the session support which would eliminate the _GET, _POST, _DELETE and _PUT methods


class States(object):
    Pending = 'pending'
    Stopped = 'stopped'
    Stopping = 'stopping'
    Running = 'running'


class Settings(object):
    NetworkConfiguration = 'networkconfiguration'
    NetworkCards = 'networkcards'
    Emulation = 'emulation'


class JobStates(object):
    Pending = 'pending'
    Succeeded = 'succeeded'
    Failed = 'failed'


class VeerezoClient(object):
    def __init__(self, server, username, password):
        self.server = server
        self.username = username
        self.password = password

        self._lastJobIDs = []


    def getImages(self):
        r = self._GET('/images')
        assert r.status_code == 200
        images = json.loads(r.content)

        return [x['image'] for x in images]


    def getSSHKeys(self):
        r = self._GET('/sshkeys')
        assert r.status_code == 200
        sshKeys = json.loads(r.content)

        return [x['sshkey'] for x in sshKeys]


    def addSSHKey(self, key):
        r = self._POST('/sshkeys', {'key': key})
        assert r.status_code == 201

        data = json.loads(r.content)
        return data['sshkey']


    def deleteSSHKey(self, id):
        r = self._DELETE(id)
        assert r.status_code == 204


    def getNetworks(self, tags=None):
        if tags:
            params = [('tag', x) for x in tags]
            params = urllib.urlencode(params)
        else:
            params = None

        r = self._GET('/networks', params=params)
        assert r.status_code == 200

        networks = json.loads(r.content)
        return [x['network'] for x in networks]


    def addNetwork(self, devices=None, tags=None):
        if devices is None:
            devices = []
        if tags is None:
            tags = []

        r = self._POST('/networks', {'devices': devices, 'tags': tags})
        assert r.status_code == 201

        data = json.loads(r.content)
        return data['network']


    def deleteNetwork(self, network):
        r = self._DELETE(network)
        assert r.status_code == 204


    def getVMs(self, tags=None):
        if tags:
            params = [('tag', x) for x in tags]
            params = urllib.urlencode(params)
        else:
            params = None

        r = self._GET('/vms', params=params)
        assert r.status_code == 200

        data = json.loads(r.content)
        return [x['vm'] for x in data]


    def addVM(self, image, ramMiB, networkCards, networkConfiguration, tags=None):
        if tags is None:
            tags = []

        d = {}
        d['image'] = image
        d['ramMiB'] = ramMiB
        d['networkCards'] = networkCards
        d['networkConfiguration'] = networkConfiguration
        d['tags'] = tags

        r = self._POST('/vms', d)
        assert r.status_code == 201

        data = json.loads(r.content)
        return data['vm']


    def deleteVM(self, vm):
        r = self._DELETE(vm)
        assert r.status_code == 204


    def startVM(self, vm):
        r = self._POST(vm + '/actions/start')
        assert r.status_code == 202


    def stopVM(self, vm):
        r = self._POST(vm + '/actions/stop')
        assert r.status_code == 202


    def killVM(self, vm):
        r = self._POST(vm + '/actions/kill')
        assert r.status_code == 202


    def rebuildVM(self, vm):
        r = self._POST(vm + '/actions/rebuild')
        assert r.status_code == 202


    def getVMState(self, vm):
        r = self._GET(vm + '/state')
        assert r.status_code == 200

        data = json.loads(r.content)
        return data['state']


    def waitForVMState(self, vm, state, timeout):
        t0 = time.time()
        while time.time() - t0 < timeout:
            if self.getVMState(vm) == state:
                return
            time.sleep(0.25)

        raise RuntimeError('Timeout while waiting for VM state.')


    def getVMSetting(self, vm, setting):
        r = self._GET(vm + '/settings/' + setting)
        assert r.status_code == 200

        data = json.loads(r.content)
        return data


    def setVMSetting(self, vm, setting, data):
        r = self._PUT(vm + '/settings/' + setting, data)
        assert r.status_code == 200


    def getJobState(self, job):
        r = self._GET(job)
        assert r.status_code == 200

        data = json.loads(r.content)
        return data['state']


    def getLastJobIDs(self):
        '''
        Returns the jobIDs associated with the last asynchronous request.

        Example:
            net = veerezo.addNetwork()
            jobIDs = veerezo.getLastJobIDs()
        '''
        return copy.copy(self._lastJobIDs)


    def _GET(self, url, params=None):
        assert url[0] == '/'

        url = '{0}{1}'.format(self.server, url)
        headers = {'Accept': 'application/json'}

        return requests.get(url, headers=headers, auth=(self.username, self.password), params=params)


    def _DELETE(self, url):
        assert url[0] == '/'

        url = '{0}{1}'.format(self.server, url)
        headers = {'Accept': 'application/json'}

        r = requests.delete(url, headers=headers, auth=(self.username, self.password))
        self._extractLastJobIDs(r)
        return r


    def _POST(self, url, data=''):
        assert url[0] == '/'

        url = '{0}{1}'.format(self.server, url)
        headers = {'Accept': 'application/json'}

        r = requests.post(url, headers=headers, data=json.dumps(data), auth=(self.username, self.password))
        self._extractLastJobIDs(r)
        return r


    def _PUT(self, url, data=''):
        assert url[0] == '/'

        url = '{0}{1}'.format(self.server, url)
        headers = {'Accept': 'application/json'}

        r = requests.put(url, headers=headers, data=json.dumps(data), auth=(self.username, self.password))
        self._extractLastJobIDs(r)
        return r


    def _extractLastJobIDs(self, response):
        if 'X-Job-IDs' in response.headers:
            s = response.headers['X-Job-IDs']
            l = s.split(',')

            self._lastJobIDs = []
            for x in l:
                x = '/jobs/{0}'.format(x)
                self._lastJobIDs.append(x)
        else:
            self._lastJobIDs = []


class VMNetworkConfigMaker(object):
    def __init__(self):
        self._dns = None
        self._ipv4 = None
        self._ipv6 = None


    def setDNS(self, servers):
        self._dns = servers
        return self


    def setIPv4Static(self, address, netmask, gateway=None):
        self._ipv4 = {
            'mode': 'static',
            'address': address,
            'netmask': netmask,
            'gateway': gateway,
        }
        return self


    def setIPv6Static(self, address, netmask, gateway=None):
        self._ipv6 = {
            'mode': 'static',
            'address': address,
            'netmask': netmask,
            'gateway': gateway,
        }
        return self


    def get(self):
        d = {}
        d['dns'] = copy.deepcopy(self._dns)
        d['ipv4'] = copy.deepcopy(self._ipv4)
        d['ipv6'] = copy.deepcopy(self._ipv6)

        return d



