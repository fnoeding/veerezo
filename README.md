
Veerezo: Virtual Networks
=========================

Veerezo is used to rapidly deploy virtual network setups with many hosts. It is similar in spirit to Amazon EC2 or OpenStack in that it provides a way to automatically create virtual machines from disk images. These virtual machines can be connected using arbitrary layer 2 network topologies.

In the future Veerezo will be able to control the interconnects between virtual machines in regard to bandwidth, packet forwarding delays, packet loss and other metrics affecting real network connections.

Veerezo is not designed to replace infrastructure as a service solutions like OpenStack or Amazon EC2. It is meant as a flexible and simple to use testbed.


Use cases
---------

* network laboratory environment: Create a virtual Internet and learn how routing protocols work
* integration testing environment for network appliances


Usage
-----

Using the python Veerezo client you can do the following:

        import pyveerezo
        
        veerezo = pyveerezo.VeerezoClient('http://veerezo-server.example:8080', 'username', 'password')

        image = veerezo.getImages()[0] # just get the "first" image
        netcfg = pyveerezo.VMNetworkConfigMaker()
        netcfg.setDNS(['8.8.8.8', '8.8.4.4']) # use the google DNS servers
        
        net = veerezo.addNetwork()
        vm1 = veerezo.addVM(ramMiB=256, image=image, networkCards=[net], networkConfiguration=netcfg.setIPv4Static('192.168.42.101', '255.255.255.0').get())
        vm2 = veerezo.addVM(ramMiB=256, image=image, networkCards=[net], networkConfiguration=netcfg.setIPv4Static('192.168.42.102', '255.255.255.0').get())
        
        veerezo.startVM(vm1)
        veerezo.startVM(vm2)
    
        veerezo.waitForVMState(vm1, pyveerezo.States.Running, 60.0)
        veerezo.waitForVMState(vm2, pyveerezo.States.Running, 60.0)
    
        raw_input('VMs are running. Wait a moment until the OS boots. If you are done press [ENTER] to stop the VMs again.')
        veerezo.deleteVM(vm1)
        veerezo.deleteVM(vm2)
        veerezo.deleteNetwork(net)

For complete examples see clients/pyveerezo/examples.


Installation and first steps
----------------------------

See INSTALL.md


RESTful API
-----------

See apidoc/API.html. A (possibly outdated) version of this document is hosted at [noeding.com/veerezo/API.html](http://noeding.com/veerezo/API.html).


Architecture
------------

Veerezo consists of the following parts:

* a backend worker process that is responsible for creating and destroying virtual networks or virtual machines
* a REST frontend to the backend worker process. This is the API entry point for end users
* a Python client to programmatically create virtual networks


The name "Veerezo"
------------------

Vee for virtual and rezo is haitian for 'network': Virtual Network.


