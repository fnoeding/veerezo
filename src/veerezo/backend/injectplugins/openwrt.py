# this code gets exec'de in scope of backend/inject.py. See _loadPlugins there.


def openwrt(vmID, authorizedKeys, networkConfig):
    def _openwrtNetworkConfig(networkConfig):
        network = '''
config interface loopback
    option ifname lo
    option proto static
    option ipaddr 127.0.0.1
    option netmask 255.0.0.0
'''

        if 'ipv4' in networkConfig:
            ipv4 = networkConfig['ipv4']
            if ipv4['mode'] == 'dhcp':
                raise NotImplementedError('dhcp IPv4 configuration not implemented')
            elif ipv4['mode'] == 'static':
                network += '''
config interface lan
    option ifname eth0
    option proto static
    option ipaddr {0}
    option netmask {1}
'''.format(ipv4['address'], ipv4['netmask'])
                if 'gateway' in ipv4:
                    network += '    option gateway {0}\n'.format(ipv4['gateway'])
            else:
                raise ValueError('invalid IPv4 mode')
            if networkConfig.get('dns', None):
                network += '    option dns {0}'.format(' '.join(networkConfig['dns']))

        network += '\n'
        return [_regularFile('/etc/config/network', network)]


    files = []
    files.extend(_getAuthorizedKeys(authorizedKeys, '/etc/dropbear/authorized_keys'))
    files.extend(_openwrtNetworkConfig(networkConfig))

    return files
_configInjectors['openwrt'] = openwrt
