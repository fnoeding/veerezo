# this code gets exec'de in scope of backend/inject.py. See _loadPlugins there.

def debian(vmID, authorizedKeys, networkConfig):
    files = []
    files.extend(_getDebianInterfaces(networkConfig))
    files.extend(_getResolvConf(networkConfig))
    files.extend(_getSSHKeyPair('dsa', '/etc/ssh/ssh_host_dsa_key'))
    files.extend(_getSSHKeyPair('rsa', '/etc/ssh/ssh_host_rsa_key'))
    files.extend(_getAuthorizedKeys(authorizedKeys))

    return files

_configInjectors['debian'] = debian
