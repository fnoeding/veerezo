import socket
import time


class KVMMonitor(object):
    def __init__(self, path):
        self._connect(path)


    def _connect(self, monitorPath):
        try:
            self._s = socket.socket(socket.AF_UNIX)
            self._s.settimeout(2) # this is a local unix socket --> timeout of 2 seconds should be ok
            self._s.connect(monitorPath)
            self._s.settimeout(None)

            self._readAll()
        except socket.error:
            return False

        return True


    def _readAll(self):
        s = []
        while True:
            time.sleep(0.1)
            try:
                r = self._s.recv(4096, socket.MSG_DONTWAIT)
                if r:
                    s.append(r)
                else:
                    break
            except socket.error:
                break

        return ''.join(s)


    def commandSimple(self, cmd):
        try:
            self._readAll()
            self._s.send(cmd + '\n')
            time.sleep(0.005)
            self._readAll()
        except socket.error:
            return False

        return True


    def close(self):
        try:
            self._s.close()
            self._s = None
        except socket.error:
            return False
        return True



