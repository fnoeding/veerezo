import os
import paramiko
from StringIO import StringIO


def inject(injectorName, rootPath, vmID, authorizedKeys, networkConfig):
    # assumes that rootPath is already mounted.
    # If the os disk image contains multiple partitions all should be mounted below the root path

    for x in _configInjectors[injectorName](vmID, authorizedKeys, networkConfig):
        if x['type'] == 'file':
            _injectRegularFile(rootPath, x)
        elif x['type'] == 'directory':
            _injectDirectory(rootPath, x)
        else:
            raise ValueError('Invalid injection type')


def _regularFile(path, data, uid=0, gid=0, mode=0644):
    d = {}
    d['type'] = 'file'
    d['path'] = path
    d['data'] = data
    d['uid'] = uid
    d['gid'] = gid
    d['mode'] = mode

    return d


def _directory(path, uid=0, gid=0, mode=0755):
    d = {}
    d['type'] = 'directory'
    d['path'] = path
    d['uid'] = uid
    d['gid'] = gid
    d['mode'] = mode

    return d


def _injectRegularFile(rootPath, d):
    if not d['type'] == 'file':
        raise ValueError('invalid injection descriptor')

    p = d['path'].lstrip('/')
    p = os.path.join(rootPath, p)

    with open(p, 'w') as f:
        f.write(d['data'])

    os.chmod(p, d['mode'])
    os.chown(p, d['uid'], d['gid'])


def _injectDirectory(rootPath, d):
    if not d['type'] == 'directory':
        raise ValueError('invalid injection descriptor')

    p = d['path'].lstrip('/')
    p = os.path.join(rootPath, p)

    if not os.path.exists(p):
        os.makedirs(p, d['mode'])
    os.chown(p, d['uid'], d['gid'])


def _getDebianInterfaces(networkConfig):
    interfaces = ['auto lo',
            'iface lo inet loopback',
            '',
            'auto eth0',
    ]

    ipv4 = networkConfig.get('ipv4', None)
    ipv6 = networkConfig.get('ipv6', None)

    if ipv4 and ipv4['mode'] == 'static':
        interfaces.append('iface eth0 inet static')
        interfaces.append('    address {0}'.format(ipv4['address']))
        interfaces.append('    netmask {0}'.format(ipv4['netmask']))
        if 'gateway' in ipv4 and ipv4['gateway']:
            interfaces.append('    gateway {0}'.format(ipv4['gateway']))
    elif ipv4 and ipv4['mode'] == 'dhcp':
        interfaces.append('iface eth0 inet dhcp')

    if ipv6 and ipv6['mode'] == 'static':
        interfaces.append('iface eth0 inet6 static')
        interfaces.append('    address {0}'.format(ipv6['address']))
        interfaces.append('    netmask {0}'.format(ipv6['netmask']))
        if 'gateway' in ipv6 and ipv6['gateway']:
            interfaces.append('    gateway {0}'.format(ipv6['gateway']))
    elif ipv6 and ipv6['mode'] == 'dhcp':
        interfaces.append('iface eth0 inet6 dhcp')
    interfaces.append('')

    interfaces = '\n'.join(interfaces)

    return [_regularFile('/etc/network/interfaces', interfaces)]


def _getResolvConf(networkConfig):
    l = []
    dns = networkConfig.get('dns', [])
    if dns is None:
        dns = []
    for x in dns:
        l.append('nameserver {0}'.format(x))
    l.append('')

    resolvConf = '\n'.join(l)

    return [_regularFile('/etc/resolv.conf', resolvConf)]


def _getSSHKeyPair(type, filename):
    if type == 'dsa':
        keygen = paramiko.DSSKey
        bits = 1024
    elif type == 'rsa':
        keygen = paramiko.RSAKey
        bits = 2048
    else:
        raise ValueError('invalid SSH key type')

    key = keygen.generate(bits=bits)

    sio = StringIO()
    key.write_private_key(sio)

    privateKey = sio.getvalue()
    publicKey = key.get_base64()

    return [_regularFile(filename, privateKey, mode=0600),
            _regularFile(filename + '.pub', publicKey, mode=0644)]


def _getAuthorizedKeys(authorizedKeys, path='/root/.ssh/authorized_keys'):
    l = [x.strip() for x in authorizedKeys]
    authorizedKeys = '\n'.join(l)

    return [_directory(os.path.split(path)[0], mode=0700),
            _regularFile(path, authorizedKeys, mode=0600)]


_configInjectors = {}
def _loadPlugins():
    path = os.path.split(__file__)[0]
    path = os.path.join(path, 'injectplugins')
    for x in os.listdir(path):
        x = os.path.join(path, x)
        if not x.endswith('.py'):
            continue

        with open(x) as f:
            code = f.read()

            exec(code)

_loadPlugins()




########################################
import unittest


class ConfigInjectionTest(unittest.TestCase):
    def test_getResolvConf(self):
        config1 = {'dns': ['8.8.8.8', '8.8.4.4']}
        expected1 = '''nameserver 8.8.8.8
nameserver 8.8.4.4
'''

        config2 = {}
        expected2 = ''

        self.assertEqual(_getResolvConf(config1)[0]['data'].strip(), expected1.strip())
        self.assertEqual(_getResolvConf(config2)[0]['data'].strip(), expected2.strip())


    def test_getDebianInterfaces(self):
        config1 = {
            'ipv4': {'mode': 'static', 'address': '192.168.42.100', 'netmask': '255.255.255.0', 'gateway': '192.168.42.1'},
            'ipv6': {'mode': 'static', 'address': 'fc00:0000:0000:0042::0100', 'netmask': '64', 'gateway': 'fc00:0000:0000:0042::0001'},
        }

        expected1 = '''auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
    address 192.168.42.100
    netmask 255.255.255.0
    gateway 192.168.42.1
iface eth0 inet6 static
    address fc00:0000:0000:0042::0100
    netmask 64
    gateway fc00:0000:0000:0042::0001
'''

        config2 = {
            'ipv4': {'mode': 'dhcp'},
            'ipv6': {'mode': 'dhcp'},
        }

        expected2 = '''auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
iface eth0 inet6 dhcp
'''

        config3 = {
        }
        expected3 = '''auto lo
iface lo inet loopback

auto eth0
'''

        self.assertEqual(_getDebianInterfaces(config1)[0]['data'].strip(), expected1.strip())
        self.assertEqual(_getDebianInterfaces(config2)[0]['data'].strip(), expected2.strip())
        self.assertEqual(_getDebianInterfaces(config3)[0]['data'].strip(), expected3.strip())




