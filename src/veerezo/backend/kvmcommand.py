

KVM = '/usr/bin/kvm'


class KVMCommand(object):
    def __init__(self):
        self.cmd = [KVM]


    def getCommand(self):
        return self.cmd


    def setCPUCount(self, n):
        self.cmd.extend(['-smp', str(n)])


    def setName(self, name):
        self.cmd.extend(['-name', name])


    def setRAM(self, MB):
        self.cmd.extend(['-m', str(MB)])


    def addDisk(self, path, index, boot=False, model='ide'):
        assert model in ['ide', 'scsi', 'virtio']

        if boot:
            boot = 'on'
        else:
            boot = 'off'

        self.cmd.append('-drive')

        s = ['file=', path,
             ',index=', str(index),
             ',media=disk',
             ',if=', model,
             ',boot=', boot,
             ',cache=none',
             ',aio=native',]
        self.cmd.append(''.join(s))


    def addNIC(self, interface, index, model, macAddress):
        if model == 'virtio': # TODO remove this code. Clients of this code should use proper values at all times
            model = 'virtio-net-pci'

        # TODO detect vhost support or use a special model name
        # vhost for virtio can be accelerated using a kernel space driver. Need 'modprobe vhost_net' for this to work
        #if model == 'virtio-net-pci':
        #    vhost = ',vhost=on'
        #else:
        #    vhost = ''
        vhost = ''

        self.cmd.append('-netdev')
        self.cmd.append('type=tap,script=no,downscript=no,ifname={0},id=netdev{1}{2}'.format(interface, index, vhost))


        self.cmd.append('-device')
        self.cmd.append('{0},netdev=netdev{1},mac={2}'.format(model, index, macAddress))


    def setDaemon(self, path):
        self.cmd.extend([
            '-daemonize',
            '-pidfile', path])


    def enableMonitor(self, path):
        self.cmd.extend([
            '-monitor', 'unix:{0},server,nowait'.format(path)])


    def disableVGAAndSetupSerialConsole(self, path):
        # must be used together to disable serial console redirection to
        # stdin / stdout of controlling terminal
        self.cmd.extend(['-nographic',
                         '-serial', 'unix:{0},server,nowait'.format(path)])


    def enableVNC(self, path):
        self.cmd.extend(['-vnc', 'unix:{0}'.format(path),
                         '-usbdevice', 'tablet']) # the tablet should improve "mouse" emulation in vnc


    def __repr__(self):
        return str(self)


    def __str__(self):
        return ' '.join(self.cmd)




