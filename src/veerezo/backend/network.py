from lowleveltools import *
import re
import logging
import time


_logger = logging.getLogger('network')



_BridgeNamePrefix = 'rezobr' # be careful: bridge names cannot be arbitrarily long
_TapNamePrefix = 'rezotap'


def configureNetworks(networkConfig):
    '''
    Configures the system to match the wanted network configuration.

    This function only
    - adds networks
    - deletes networks
    - ensures that all physical devices (ethX, ethX.Y) are attached to the correct networks
    This function does NOT touch the network configuration of any vserver.
    '''

    try:
        t0 = time.time()

        existingNetworks = getNetworks()

        existingIDs = set(existingNetworks.keys())
        wantedIDs = set(networkConfig.keys())

        _deleteNetworks(existingIDs - wantedIDs)
        _addNetworks(wantedIDs - existingIDs)

        _detachUnwantedDevicesFromNetworks(networkConfig, existingNetworks, wantedIDs)
        _attachWantedDevicesToNetworks(networkConfig, existingNetworks, wantedIDs)

        dt = time.time() - t0
        _logger.info('Reconfigured network ({0:.1f}s).'.format(dt))
    except:
        _logger.error('Failed to reconfigure network.')
        raise


def _deleteNetworks(IDs):
    for x in IDs:
        name = _getBridgeName(x)
        try:
            delBridge(name)

            msg = 'Deleted bridge "{0}".'.format(name)
            _logger.info(msg)
        except:
            msg = 'Failed to delete bridge "{0}". This may lead to other errors later.'.format(name)
            _logger.error(msg)


def _addNetworks(IDs):
    for x in IDs:
        name = _getBridgeName(x)
        try:
            addBridge(name)

            msg = 'Added bridge "{0}".'.format(name)
            _logger.info(msg)
        except:
            msg = 'Failed to add bridge "{0}". This may lead to other errors later.'.format(name)
            _logger.error(msg)


def _detachUnwantedDevicesFromNetworks(networkConfig, existingNetworks, wantedIDs):
    for x in wantedIDs:
        name = _getBridgeName(x)
        attachedDevices = set(existingNetworks.get(x, []))
        wantedDevices = set(networkConfig[x])

        delDevices = attachedDevices - wantedDevices
        for y in delDevices:
            if y.startswith(_TapNamePrefix):
                continue

            try:
                delBridgeInterface(name, y)

                msg = 'Detached interface "{0}" from bridge "{1}".'.format(y, name)
                _logger.info(msg)
            except:
                msg = 'Failed to detach interface "{0}" from bridge "{1}". This may lead to other errors later.'.format(y, name)
                _logger.error(msg)


def _attachWantedDevicesToNetworks(networkConfig, existingNetworks, wantedIDs):
    for x in wantedIDs:
        name = _getBridgeName(x)
        attachedDevices = set(existingNetworks.get(x, []))
        wantedDevices = set(networkConfig[x])

        addDevices = wantedDevices - attachedDevices
        for y in addDevices:
            if y.startswith(_TapNamePrefix):
                continue

            try:
                addBridgeInterface(name, y)
                msg = 'Attached interface "{0}" to bridge "{1}".'.format(y, name)
                _logger.info(msg)
            except:
                msg = 'Failed to attach interface "{0}" to bridge "{1}". This may lead to other errors later.'.format(y, name)
                _logger.error(msg)



def getNetworks():
    pattern = re.compile(_getBridgeName(r'(\d+)'))
    existingNetworks = {}
    for name, devices in getBridges().iteritems():
        m = pattern.match(name)
        if not m:
            continue

        id = m.groups()[0]
        id = int(id)

        existingNetworks[id] = devices

    return existingNetworks


def _getBridgeName(id):
    return '{0}{1}'.format(_BridgeNamePrefix, id)


def configureVServerTaps(vmID, networks):
    '''
    Setups tap devices for a vserver and connects them to the correct network.

    In case of errors: This function calls removeVServerTaps internally to cleanup any added devices. Then raises.
    '''
    try:
        taps = []
        for i, network in enumerate(networks):
            tapName = _getTapName(vmID, i)
            taps.append(tapName)
            addTapDevice(tapName)

            if network is not None:
                networkName = _getBridgeName(network)
                addBridgeInterface(networkName, tapName)
            _logger.info('Added tap device "{0}" for vserver "vm{1}" attached to network "{2}".'.format(tapName, vmID, networkName))
    except:
        _logger.error('Failed to add at least one tap device for vserver "vm{0}". Cleaning up again...'.format(vmID))
        removeVServerTaps(vmID)
        raise

    return taps


def removeVServerTaps(vmID):
    '''
    Removes all tap devices setup by configureVServerTaps.

    Does not raise when a tap device could not get deleted.
    '''
    existingTaps = getTapDevices()

    pattern = re.compile(r'^{0}{1}[a-z]$'.format(_TapNamePrefix, vmID))
    for x in existingTaps:
        if pattern.match(x):
            try:
                delTapDevice(x)
                _logger.info('Deleted tap device "{0}".'.format(x))
            except:
                msg = 'Failed to delete tap device "{0}". This may lead to other errors later.'.format(x)
                _logger.error(msg)


def _getTapName(vmID, index):
    # XXX this naming scheme limits us to 26 NICs per VM. Should be enough for now...
    assert 0 <= index < 26
    suffix = chr(ord('a') + index)
    return '{0}{1}{2}'.format(_TapNamePrefix, vmID, suffix)


