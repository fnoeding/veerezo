import validictory

validate = validictory.validate


VMSettingsEmulation = {
    'type': 'object',
    'properties': {
        'ramMiB': {'type': 'integer', 'minimum': 32},
        'diskModel': {'enum': ['ide', 'scsi', 'virtio']},
        'nicModel': {'enum': 'ne2k_pci,i82551,i82557b,i82559er,rtl8139,e1000,pcnet,virtio'.split(',')},
    },
    'additionalProperties': False,
}


VMSettingsNetworkCards = {
    'type': 'array',
    'items': {'type': ['null', 'string'], 'pattern': r'^/networks/\d+$'},
    'minItems': 1,
    'maxItems': 8, # TODO check this limit against the KVM / qemu docs
}


VMSettingsNetworkConfigurationIP46DHCP = {
    'type': 'object',
    'properties': {
        'mode': {'type': 'string', 'pattern': r'^dhcp$'},
    },
    'additionalProperties': False,
}
VMSettingsNetworkConfigurationIP46Static = { # XXX only checks that address, netmask and gateway are strings, not IP addresses
    'type': 'object',
    'properties': {
        'mode': {'type': 'string', 'pattern': r'^static$'},
        'address': {'type': 'string'},
        'netmask': {'type': 'string'},
        'gateway': {'type': ['string', 'null'], 'required': False},
    },
    'additionalProperties': False,
}
VMSettingsNetworkConfigurationIP46Unconfigured = {
    'type': ['object', 'null'],
    'properties': {},
    'additionalProperties': False,
}
VMSettingsNetworkConfigurationIP46 = {
    'type': [VMSettingsNetworkConfigurationIP46DHCP, VMSettingsNetworkConfigurationIP46Static, VMSettingsNetworkConfigurationIP46Unconfigured],
    'required': False,
}
VMSettingsNetworkConfiguration = {
    'type': 'object',
    'properties': {
        'dns': {'type': 'array', 'items': {'type': 'string'}, 'required': False},
        'ipv4': VMSettingsNetworkConfigurationIP46,
        'ipv6': VMSettingsNetworkConfigurationIP46,
    },
    'additionalProperties': False,
}


AddSSHKey = {
    'type': 'object',
    'properties': {
        'key': {'type': 'string'}, # XXX only checks that the key is a string, not a valid ssh public key
    },
    'additionalProperties': False,
}


AddNetwork = {
    'type': 'object',
    'properties': {
        'devices': {'type': 'array', 'items': {'type': 'string'}, 'required': False},
        'tags': {'type': 'array', 'items': {'type': 'string'}, 'required': False},
    },
    'additionalProperties': False,
}


AddVM = {
    'type': 'object',
    'properties': {
        'tags': {'type': 'array', 'items': {'type': 'string'}, 'required': False},
        'image': {'type': 'string', 'pattern': r'^/images/\d+$'},
        'ramMiB': {'type': 'integer', 'minimum': 32},
        'networkConfiguration': VMSettingsNetworkConfiguration,
        'networkCards': VMSettingsNetworkCards,
    },
    'additionalProperties': False,
}




import unittest

class VMSettings(unittest.TestCase):
    def test_AddSSHKey(self):
        self.assertRaises(ValueError, validate, {}, AddSSHKey)
        self.assertRaises(ValueError, validate, {'key': 42}, AddSSHKey)
        self.assert_(validate({'key': 'somekey'}, AddSSHKey) is None)


    def test_AddNetwork(self):
        self.assertRaises(ValueError, validate, {'whatever': 42}, AddNetwork)
        self.assert_(validate({}, AddNetwork) is None)
        self.assert_(validate({'devices': ['eth0']}, AddNetwork) is None)
        self.assert_(validate({'tags': ['a', 'b', 'c']}, AddNetwork) is None)
        self.assert_(validate({'devices': ['eth0', 'eth1'], 'tags': ['a', 'b', 'c']}, AddNetwork) is None)


    def test_VMSettingsEmulation(self):
        bad = [
            'xxx',
            {},
            [],
            #42, # raises TypeError, bug in validictory?
            {'diskModel': 'ide', 'nicModel': 'virtio'},
            {'ramMiB': 256, 'nicModel': 'virtio'},
            {'ramMiB': 256, 'diskModel': 'ide'},
            {'ramMiB': 0, 'diskModel': 'ide', 'nicModel': 'virtio'},
            {'ramMiB': '256', 'diskModel': 'ide', 'nicModel': 'virtio'},
        ]

        good = [
            {'ramMiB': 256, 'diskModel': 'ide', 'nicModel': 'virtio'},
        ]

        for x in bad:
            self.assertRaises(ValueError, validate, x, VMSettingsEmulation)
        for x in good:
            self.assert_(validate(x, VMSettingsEmulation) is None)


    def test_VMSettingsNetworkCards(self):
        bad = [
            [],
            [1],
            ['/networks/abc'],
        ]

        good = [
            [None],
            [None, None],
            ['/networks/1'],
            ['/networks/1', '/networks/42'],
            ['/networks/1', '/networks/2', '/networks/1'], # XXX we do not detect duplicate entries, since we need to allow multiple None entries
        ]
        for x in bad:
            self.assertRaises(ValueError, validate, x, VMSettingsNetworkCards)
        for x in good:
            self.assert_(validate(x, VMSettingsNetworkCards) is None)


    def test_VMSettingsNetworkConfiguration(self):
        bad = [
            {'whatever': 42},
            {'dns': '8.8.8.8'},
        ]
        good = [
            {},
            {'dns': []},
            {'dns': ['8.8.8.8', '8.8.4.4']},
            {'ipv4': {'mode': 'static', 'address': '192.168.42.100', 'netmask': '255.255.255.0', 'gateway': '192.168.42.1'}},
            {'ipv6': {'mode': 'static', 'address': 'fc00::0100', 'netmask': '64', 'gateway': 'fc00::0001'}},
        ]

        for x in bad:
            self.assertRaises(ValueError, validate, x, VMSettingsNetworkConfiguration)
        for x in good:
            self.assert_(validate(x, VMSettingsNetworkConfiguration) is None)


    def test_VMSettingsNetworkConfigurationIP(self):
        bad = [
            {'mode': 'dhcp', 'gateway': '192.168.42.1'},
        ]
        good = [
            {'mode': 'dhcp'},
            {'mode': 'static', 'address': '192.168.42.100', 'netmask': '255.255.255.0', 'gateway': '192.168.42.1'},
            {'mode': 'static', 'address': 'fc00::0100', 'netmask': '64', 'gateway': 'fc00::0001'},
        ]
        for x in bad:
            self.assertRaises(ValueError, validate, x, VMSettingsNetworkConfigurationIP46)
        for x in good:
            self.assert_(validate(x, VMSettingsNetworkConfigurationIP46) is None)


    def test_VMSettingsNetworkConfiguration(self):
        bad = [
            {'whatever': 'foo'},
        ]
        good = [
            {},
            {'dns': []},
            {'ipv4': None, 'ipv6': None},
            {'ipv4': {'mode': 'dhcp'}},
            {'ipv4': {'mode': 'static', 'address': '192.168.42.100', 'netmask': '255.255.255.0', 'gateway': '192.168.42.1'}},
            {'ipv6': {'mode': 'static', 'address': 'fc00::0100', 'netmask': '64', 'gateway': 'fc00::0001'}},
        ]
        for x in bad:
            self.assertRaises(ValueError, validate, x, VMSettingsNetworkConfiguration)
        for x in good:
            self.assert_(validate(x, VMSettingsNetworkConfiguration) is None)


    def test_AddVM(self):
        good = [
            {
                'image': '/images/1',
                'tags': ['a', 'b', 'c'],
                'networkCards': ['/networks/1', None, None, None],
                'networkConfiguration': {'dns': ['8.8.8.8', '8.8.4.4'], 'ipv4': {'mode': 'dhcp'}},
                'ramMiB': 256,
            },
        ]
        for x in good:
            self.assert_(validate(x, AddVM) is None)


