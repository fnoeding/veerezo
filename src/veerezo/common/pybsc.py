#!/usr/bin/python

# 
# The BSD License
# 
# Copyright (c) 2009, Florian Noeding <florian@noeding.com>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
# Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or
# other materials provided with the distribution.
# Neither the name of the of the author nor the names of its contributors may be
# used to endorse or promote products derived from this software without specific
# prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 


__version__ = '0.0.1' # implements beanstalkd protocol version: 1.3

import socket
import time
import types
import unittest
import errno

import yaml # beanstalkd returns some data in yaml format...


class Error(Exception): pass
class OOMError(Error): pass
class DrainingError(Error): pass
class JobTooBigError(Error): pass
class JobBuriedError(Error):
	def __init__(self, jobID=None):
		self.jobID = jobID
class JobNotFoundError(Error): pass
class DeadlineSoonError(Error): pass
class TimedOutError(Error): pass
class ConnectionError(Error): pass

class InternalServerError(Error): pass
class InternalError(Error): pass
class UnexpectedError(Error): pass # sometimes this error is raised even though it really is a ConnectionError


_BeanstalkD = '/usr/bin/beanstalkd'



class BeanstalkClient(object):
	''' beanstalkd client
	Attention: the beanstalk protocol is NOT stateless. After a connection error and reconnect
	you have to issue the necessary use / watch / ignore commands again!
	'''

	def __init__(self, host='127.0.0.1', port=11300, connect=True):
		self._conn = _Conn(host, port)

		if connect:
			self.connect()

	def connect(self):
		if not self._conn.connect():
			raise ConnectionError('could not connect')


	def close(self):
		self._conn.close()


	def _sendCommand(self, cmd):
		try:
			self._conn.sendCommand(cmd)
		except:
			raise Error()


	def _getResult(self):
		try:
			line = self._conn.readLine()
		except:
			raise Error()

		if line == 'OUT_OF_MEMORY':
			raise OOMError()
		elif line == 'INTERNAL_ERROR':
			raise InternalServerError()
		elif line == 'BAD_FORMAT' or line == 'UNKNOWN_COMMAND':
			raise InternalError()
		elif line == 'DRAINING':
			raise DrainingError()
		elif line == '':
			raise ConnectionError()

		return line


	def _getData(self, nBytes):
		try:
			data = self._conn.readData(nBytes)
		except:
			raise Error()

		if len(data) != nBytes:
			raise UnexpectedError()

		return data


	def put(self, data, priority=2147483648, delay=0, ttr=60, tube=None):
		''' returns job ID on success'''

		if tube is not None:
			self.use(tube)

		cmd = 'put %d %d %d %d\r\n%s' % (priority, delay, ttr, len(data), data)
		self._sendCommand(cmd)

		line = self._getResult()
		if line.startswith('INSERTED '):
			line = line.split()
			return int(line[1])
		elif line == 'JOB_TOO_BIG':
			raise JobTooBigError()
		elif line == 'EXPECTED_CRLF':
			raise InternalError('should never happen')
		elif line.startswith('BURIED '):
			line = line.split()
			jobID = int(line[1])
			raise JobBuriedError(jobID)
		else:
			raise UnexpectedError()


	def use(self, tube):
		''' returns None '''

		cmd = 'use %s' % tube
		self._sendCommand(cmd)

		line = self._getResult()
		if line != 'USING %s' % tube:
			raise UnexpectedError()


	def reserve(self, timeout=None):
		''' returns (jobID, data) '''

		if timeout is None:
			self._sendCommand('reserve')
		else:
			self._sendCommand('reserve-with-timeout %d' % timeout)

		line = self._getResult()
		if line.startswith('RESERVED '):
			line = line.split()
			jobID = int(line[1])
			data = self._getData(int(line[2]))

			return jobID, data
		elif line == 'DEADLINE_SOON':
			raise DeadlineSoonError()
		elif line == 'TIMED_OUT':
			raise TimedOutError()
		else:
			raise UnexpectedError()


	def delete(self, jobID):
		cmd = 'delete %d' % jobID
		self._sendCommand(cmd)

		line = self._getResult()
		if line == 'DELETED':
			return
		elif line == 'NOT_FOUND':
			raise JobNotFoundError()
		else:
			raise UnexpectedError()


	def watch(self, tube):
		cmd = 'watch %s' % tube
		self._sendCommand(cmd)

		line = self._getResult()
		if line.startswith('WATCHING '):
			return
		else:
			raise UnexpectedError()

	def ignore(self, tube):
		cmd = 'ignore %s' % tube
		self._sendCommand(cmd)

		line = self._getResult()
		if line.startswith('WATCHING '):
			return
		elif line == 'NOT_IGNORED':
			raise Error('must not ignore only watched tube') # use dedicated error type?
		else:
			raise UnexpectedError()


	def release(self, jobID, priority=2147483648, delay=0):
		cmd = 'release %s %s %s' % (jobID, priority, delay)
		self._sendCommand(cmd)

		line = self._getResult()
		if line == 'RELEASED':
			return
		elif line == 'BURIED':
			raise JobBuriedError(jobID)
		elif line == 'NOT_FOUND':
			raise JobNotFoundError()
		else:
			raise UnexpectedError()


	def bury(self, jobID, priority=2147483648):
		cmd = 'bury %s %s' % (jobID, priority)
		self._sendCommand(cmd)

		line = self._getResult()
		if line == 'BURIED':
			return
		elif line == 'NOT_FOUND':
			raise JobNotFoundError()
		else:
			raise UnexpectedError()


	def kick(self, maxJobs=1):
		cmd = 'kick %s' % maxJobs
		self._sendCommand(cmd)

		line = self._getResult()
		if line.startswith('KICKED '):
			line = line.split()
			return int(line[1])
		else:
			raise UnexpectedError()


	def touch(self, jobID):
		cmd = 'touch %s' % jobID
		self._sendCommand(cmd)

		line = self._getResult()
		if line == 'TOUCHED':
			return
		elif line == 'NOT_FOUND':
			raise JobNotFoundError()
		else:
			raise UnexpectedError()


	def stats(self, jobID=None, tube=None):
		if jobID and tube:
			raise AssertionError('jobID and tube must not be used at the same time')

		if jobID:
			self._sendCommand('stats-job %s' % jobID)
		elif tube:
			self._sendCommand('stats-tube %s' % tube)
		else:
			self._sendCommand('stats')

		line = self._getResult()
		if line.startswith('OK '):
			line = line.split()
			nBytes = int(line[1])

			data = self._getData(nBytes)
			return yaml.load(data)
		else:
			if line == 'NOT_FOUND' and (jobID or tube):
				raise JobNotFoundError()
			else:
				raise UnexpectedError()

	def listTubes(self):
		self._sendCommand('list-tubes')

		line = self._getResult()
		if line.startswith('OK '):
			line = line.split()
			nBytes = int(line[1])
			data = self._getData(nBytes)

			l = yaml.load(data)
			for i in range(len(l)):
				l[i] = str(l[i]) # if a tube is name '42' yaml returns an int, not a string
			return l
		else:
			raise UnexpectedError()


	def listTubeUsed(self):
		self._sendCommand('list-tube-used')

		line = self._getResult()
		if line.startswith('USING '):
			line = line.split()
			return line[1]
		else:
			raise UnexpectedError()


	def listTubesWatched(self):
		self._sendCommand('list-tubes-watched')

		line = self._getResult()
		if line.startswith('OK '):
			line = line.split()
			nBytes = int(line[1])
			data = self._getData(nBytes)

			l = yaml.load(data)
			for i in range(len(l)):
				l[i] = str(l[i]) # if a tube is name '42' yaml returns an int, not a string
			return l
		else:
			raise UnexpectedError()


	def peek(self, jobID=None, jobType=None):
		''' returns (jobID, data) '''

		if jobID is None and jobType is None:
			raise AssertionError('jobID and jobType must not both be None')
		elif jobID and jobType:
			raise AssertionError('jobID and jobType must not be both not-None')
		if jobType and jobType not in ['ready', 'delayed', 'buried']:
			raise AssertionError('invalid job jobType')

		if jobID:
			self._sendCommand('peek %s' % jobID)
		else:
			self._sendCommand('peek-%s' % jobType)

		line = self._getResult()
		if line.startswith('FOUND '):
			line = line.split()
			jobID = int(line[1])
			nBytes = int(line[2])
			data = self._getData(nBytes)
			return (jobID, data)
		elif line == 'NOT_FOUND':
			raise JobNotFoundError()
		else:
			raise UnexpectedError()






class _Conn(object):
	def __init__(self, host='127.0.0.1', port=11300):
		self.host = host
		self.port = port

		self.socketTimeout = 3 # seconds before socket timeout
		self.socketDeadTime = 0 # seconds until a dead socket may connect again


		self.socket = None
		self.deadUntil = 0
		self.deadMessage = ''


	def _markDead(self, message):
		self.deadUntil = time.time() + self.socketDeadTime

		self.close()

	def isDead(self):
		return self.deadUntil > time.time()


	def _getSocket(self):
		if self.isDead():
			return None

		if self.socket:
			return self.socket

		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket = s
		s.settimeout(self.socketTimeout)

		try:
			s.connect((self.host, self.port))
		except socket.timeout, message:
			self._markDead('connect: %s' % message)
			return None
		except socket.error, message:
			self._markDead('connect: errno=%s' % message.errno)
			return None

		self.buffer = ''

		return s



	def connect(self):
		if self._getSocket():
			return True
		else:
			return False


	def close(self):
		if self.socket:
			try:
				self.socket.close()
			except:
				pass
		self.socket = None


	def readLine(self):
		buf = self.buffer
		recv = self.socket.recv

		while True:
			idx = buf.find('\r\n')
			if idx >= 0:
				break

			try:
				data = recv(4096)
			except socket.timeout:
				self._markDead('readLine (recv): connection dropped while reading data')
				break
			except socket.error, message:
				if message.errno == errno.EINTR:
					continue
				else:
					self._markDead('readLine (recv): errno=%s' % message.errno)
					break

			if not data:
				self._markDead('readLine (recv): connection closed while reading data')
				break
			buf += data

		if idx >= 0:
			self.buffer = buf[idx + 2:]
			buf = buf[:idx]
		else:
			self.buffer = ''

		return buf


	def readData(self, nBytes):
		buf = self.buffer
		recv = self.socket.recv

		n = 0
		while len(buf) < nBytes + 2:
			try:
				data = recv(nBytes - len(buf))
			except socket.timeout:
				self._markDead('readData (recv): connection dropped while reading data')
				break
			except socket.error, message:
				if message.errno == errno.EINTR:
					continue
				else:
					self._markDead('readData (recv): errno=%s' % message.errno)
					break

			if not data:
				self._markDead('readData (recv): connection colsed while reading data')
				break

			n += 1
			if n > nBytes:
				raise AssertionError('readData failed: beanstalkd returned not enough data')
		self.buffer = ''

		return buf[:-2] # trailing '\r\n'


	def sendCommand(self, cmd):
		self.socket.sendall(cmd + '\r\n') # XXX does this really handle errno.EINTR?




class Test_Conn(unittest.TestCase):
	def setUp(self):
		import subprocess as sp

		host = '127.0.0.1'
		port = 63130
		self.host = host
		self.port = port

		self.bsd = sp.Popen([_BeanstalkD, '-l', host, '-p', '%s' % port], stderr=file('/dev/null', 'w'), stdout=file('/dev/null', 'w'))
		time.sleep(0.5)

		self.conn = _Conn(host, port)

	def tearDown(self):
		import os
		if self.bsd:
			os.kill(self.bsd.pid, 15) # TERMinate beanstalkd

		if os.getuid() == 0:
			os.system('iptables -D INPUT -p tcp --dport %s -j DROP 2> /dev/null' % self.port)
			os.system('iptables -D INPUT -p tcp --sport %s -j DROP 2> /dev/null' % self.port)


	def test_connect(self):
		conn = self.conn
		self.assert_(conn.connect())


	def test_connectFail(self):
		conn = _Conn('127.0.0.1', 1)
		self.assert_(not conn.connect())


	def test_connectFail2_NeedRoot(self):
		import os
		self.assert_(os.getuid() == 0)

		os.system('iptables -A INPUT -p tcp --dport %s -j DROP 2> /dev/null' % self.port)
		os.system('iptables -A INPUT -p tcp --sport %s -j DROP 2> /dev/null' % self.port)

		conn = _Conn(self.host, self.port)
		self.assert_(not conn.connect())



	def test_sendCommand_readLine(self):
		conn = self.conn
		conn.connect()
		conn.sendCommand('stats') # expected data: OK <bytes>\r\n<data>\r\n

		line = conn.readLine()
		self.assert_(line)
		self.assert_(line.startswith('OK '))

		line = line.split()
		nBytes = int(line[1])

		data = conn.readData(nBytes)
		self.assert_(len(data) == nBytes)



class Test_BeanstalkClient(unittest.TestCase):


	def setUp(self):
		import subprocess as sp

		host = '127.0.0.1'
		port = 63130
		self.host = host
		self.port = port


		self.bsd = sp.Popen([_BeanstalkD, '-l', host, '-p', '%s' % port], stderr=file('/dev/null', 'w'), stdout=file('/dev/null', 'w'))
		time.sleep(0.5)

		self.bs = BeanstalkClient(host, port)
		self.bs.connect()

	def tearDown(self):
		import os
		if self.bsd:
			os.kill(self.bsd.pid, 15) # TERMinate beanstalkd

		if os.getuid() == 0:
			os.system('iptables -D INPUT -p tcp --dport %s -j DROP 2> /dev/null' % self.port)
			os.system('iptables -D INPUT -p tcp --sport %s -j DROP 2> /dev/null' % self.port)



	def test_put(self):
		bs = self.bs

		r = bs.put('test')
		self.assert_(isinstance(r, int))

		data = 'x' * 1024 * 1024
		self.assertRaises(JobTooBigError, bs.put, data)

		bs.close()


	def test_use(self):
		bs = self.bs

		bs.use('a')
		bs.use('whatever')
		bs.use('42')

		bs.close()


	def test_reserve(self):
		bs = self.bs

		data = 'test'
		jobID = bs.put(data)

		r = bs.reserve(1)
		self.assert_(r is not None)
		self.assert_(r[0] == jobID)
		self.assert_(r[1] == data)

		self.assertRaises(TimedOutError, bs.reserve, 1)


	def test_use2(self):
		bs = self.bs

		bs.use('default')
		d1 = 'test'
		j1 = bs.put(d1)

		bs.use('42') # use does not change the watched tubes list
		d2 = 'x'
		j2 = bs.put(d2)

		r = bs.reserve(1)
		self.assert_(r[0] == j1)
		self.assert_(r[1] == d1)

		self.assertRaises(TimedOutError, bs.reserve, 1)


	def test_delete(self):
		bs = self.bs

		j = bs.put('test')

		r = bs.reserve(1)
		self.assert_(r[0] == j)

		bs.delete(j)
		self.assertRaises(JobNotFoundError, bs.delete, j)


	def test_watch(self):
		bs = self.bs

		j = bs.put('test')

		bs.use('42')
		j2 = bs.put('answer')

		bs.watch('42')

		r = bs.reserve(1)
		self.assert_(r[0] in [j, j2])

		r = bs.reserve(1)
		self.assert_(r[0] in [j, j2])

		self.assertRaises(TimedOutError, bs.reserve, 1)


	def test_ignore(self):
		bs = self.bs

		j = bs.put('test')

		bs.use('42')
		j2 = bs.put('answer')

		bs.watch('42')
		bs.ignore('default')

		r = bs.reserve(1)
		self.assert_(r[0] == j2)

		self.assertRaises(TimedOutError, bs.reserve, 1)


	def test_release(self):
		bs = self.bs

		j = bs.put('test')
		r = bs.reserve(1)
		self.assert_(r[0] == j)

		bs.release(j)
		self.assertRaises(JobNotFoundError, bs.release, j)

		r = bs.reserve(1)
		self.assert_(r[0] == j)


	def test_bury(self):
		bs = self.bs

		j = bs.put('test')
		r = bs.reserve(1)
		self.assert_(r[0] == j)

		bs.bury(j)
		self.assertRaises(JobNotFoundError, bs.bury, j + 1)

		self.assertRaises(TimedOutError, bs.reserve, 1)


	def test_kick(self):
		bs = self.bs

		j = bs.put('test')
		r = bs.reserve(1)
		self.assert_(r[0] == j)
		bs.bury(j)

		self.assertRaises(TimedOutError, bs.reserve, 1)
		self.assertEqual(1, bs.kick(10))
		r = bs.reserve(1)
		self.assert_(r[0] == j)


	def test_touch(self):
		bs = self.bs

		j = bs.put('test')
		r = bs.reserve(1)
		self.assertEqual(r[0], j)

		bs.touch(j)


	def test_stats(self):
		bs = self.bs

		s = bs.stats()
		self.assert_(isinstance(s, dict))

		s = bs.stats(tube='default')
		self.assert_(isinstance(s, dict))

		j = bs.put('test')
		s = bs.stats(jobID=j)
		self.assert_(isinstance(s, dict))



		self.assertRaises(JobNotFoundError, bs.stats, jobID=j+100)
		self.assertRaises(JobNotFoundError, bs.stats, tube='x')


	def test_listTubes(self):
		bs = self.bs

		l = bs.listTubes()
		self.assertEqual(l, ['default'])

		bs.use('42')
		bs.put('answer')

		l = bs.listTubes()
		self.assertEqual(len(l), 2)
		self.assert_('42' in l)
		self.assert_('default' in l)


	def test_listTubeUsed(self):
		bs = self.bs

		self.assertEqual(bs.listTubeUsed(), 'default')
		bs.use('42')
		self.assertEqual(bs.listTubeUsed(), '42')


	def test_listTubesWatched(self):
		bs = self.bs

		self.assertEqual(bs.listTubesWatched(), ['default'])
		bs.watch('42')
		l = bs.listTubesWatched()
		self.assertEqual(len(l), 2)
		self.assert_('42' in l)
		self.assert_('default' in l)

		bs.ignore('default')
		self.assertEqual(bs.listTubesWatched(), ['42'])


	def test_peek(self):
		bs = self.bs

		self.assertRaises(JobNotFoundError, bs.peek, jobID=1)
		self.assertRaises(JobNotFoundError, bs.peek, jobType='ready')
		self.assertRaises(JobNotFoundError, bs.peek, jobType='delayed')
		self.assertRaises(JobNotFoundError, bs.peek, jobType='buried')

		j = bs.put('test', delay=1)
		r = bs.peek(jobType='delayed')
		self.assertEqual(r[0], j)
		self.assertRaises(JobNotFoundError, bs.peek, jobType='ready')
		self.assertRaises(JobNotFoundError, bs.peek, jobType='buried')








#missing:
#	peek, peek-ready, peek-delayed, peek-buried
#

	def test_serverGoesAway(self):
		bs = self.bs

		j = bs.put('test')
		r = bs.reserve(1)
		self.assert_(r[0] == j)

		import os
		if self.bsd:
			os.kill(self.bsd.pid, 15) # TERMinate beanstalkd

		time.sleep(0.5)
		self.assertRaises(ConnectionError, bs.put, 'test')


	def test_serverGoesAway2(self):
		bs = self.bs

		import subprocess as sp
		killer = sp.Popen(['/bin/bash', '-c', 'sleep 1; kill -15 %s' % self.bsd.pid], stdout=file('/dev/null', 'w'), stderr=file('/dev/null', 'w')) # TERMinate

		self.assertRaises(ConnectionError, bs.reserve)


	def test_connectionDropsEverything_NeedsRoot(self):
		bs = self.bs

		# make sure that iptables is not currently blocking us
		self.assertRaises(TimedOutError, bs.reserve, 1)

		import os
		self.assert_(os.getuid() == 0)

		import subprocess as sp
		killer = sp.Popen(['/bin/bash', '-c', 'sleep 1; iptables -A INPUT -p tcp --dport %s -j DROP; iptables -A INPUT -p tcp --sport %s -j DROP' % (self.port, self.port)])#, stdout=file('/dev/null', 'w'), stderr=file('/dev/null', 'w'))

		self.assertRaises(ConnectionError, bs.reserve)


	def test_connectionDropsEverything2_NeedsRoot(self):
		bs = self.bs

		# make sure that iptables is not currently blocking us
		self.assertRaises(TimedOutError, bs.reserve, 1)

		import os
		self.assert_(os.getuid() == 0)


		os.system('iptables -A INPUT -p tcp --dport %s -j DROP 2> /dev/null' % self.port)
		os.system('iptables -A INPUT -p tcp --sport %s -j DROP 2> /dev/null' % self.port)

		self.assertRaises(ConnectionError, bs.put, 'test')









if __name__ == '__main__':
	unittest.main()



