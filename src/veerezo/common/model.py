import couchdb
import random
import time


def makeDesign():
    design = {}
    views = {}

    design['validate_doc_update'] = '''function(newDoc, oldDoc, userCtx) {

    var checkIDTypes = ["job", "image", "network", "sshkey", "vm"];
    if(checkIDTypes.indexOf(newDoc.type) != -1) {
        var pattern = "^" + newDoc.type + "-\\\\d+$";
        var re = new RegExp(pattern);
        var m = re.exec(newDoc._id);
        if(m == null) {
            throw({forbidden: "Invalid document _id for type '" + newDoc.type + "'."});
        }
    }
}'''


    design['views'] = views

    map = '''function(doc) {
    if(doc.type == "job") {
        emit(doc.state, null);
    }
}'''
    views['jobs_by_state'] = {'map': map}

    map = '''function(doc) {
    if(doc.type == "image") {
        emit(doc._id, null);
    }
}'''
    views['images_by_id'] = {'map': map}


    map = '''function(doc) {
    if(doc.type == "network") {
        emit(doc._id, null);
    }
}'''
    views['networks_by_id'] = {'map': map}


    map = '''function(doc) {
    if(doc.type == "network") {
        emit(doc.user, doc.tags);
    }
}'''
    views['networks_by_user'] = {'map': map}


    map = '''function(doc) {
    if(doc.type == "sshkey") {
        emit(doc._id, null);
    }
}'''
    views['sshkeys_by_id'] = {'map': map}


    map = '''function(doc) {
    if(doc.type == "sshkey") {
        emit(doc.user, null);
    }
}'''
    views['sshkeys_by_user'] = {'map': map}


    map = '''function(doc) {
    if(doc.type == "vm")
    {
        emit(doc._id, null);
    }
}'''
    views['vms_by_id'] = {'map': map}


    map = '''function(doc) {
    if(doc.type == "vm") {
        emit(doc.user, doc.tags);
    }
}'''
    views['vms_by_user'] = {'map': map}


    map = '''function(doc) {
    if(doc.type == "vmstate")
    {
        emit(doc._id, null);
    }
}'''
    views['vmstates_by_id'] = {'map': map}


    map = '''function(doc) {
    if(doc.type == "vmstate") {
        emit(doc.state, null);
    }
}'''
    views['vmstates_by_state'] = {'map': map}

    return design



def getDBConnection():
    couch = couchdb.Server()
    return couch['veerezo']


def updateDesign(db):
    if '_design/veerezo' in db:
        oldDesign = db['_design/veerezo']
        db.delete(oldDesign)

    db['_design/veerezo'] = makeDesign()


def addDocumentUsingPrefixPlusUniqueID(db, prefix, data, maxTries=100):
    '''
    returns tuple(_id, _rev) of saved document
    '''
    # why do we need this?
    # the backend needs unique integers to identify devices / processes in a stateless way:
    #     LVM block device names are derived from the unique virtual machine id
    #     networks are identified by a unique id
    #     kvm / qemu processes are assigned a unique id
    #     ...
    #
    # basic idea:
    # create random integers to be used as document ids until we find a free spot. couchdb ensures that the ids are unique.
    # attention: this concept will break if replication together with a master-master scenario is used! (use an external way to create unique IDs)
    assert '-' not in prefix

    # interface names are limited in length to IFNAMSIZ-1, that is usually 15 characters
    # --> by default we are using the following network device naming scheme:
    #     rezobr(\d+)
    #     rezotap(\d+)([a-z]) --> 7 chars prefix, 1 char suffix --> 7 digits
    # if a bigger key space is necessary: use base 36 encoding of IDs in network devices

    # anyway: it's not realistic to have more than a few thousand networks / virtual machines
    # so the key space should only be populated sparsely.
    minID = 1
    maxID = 9999999

    for _ in range(maxTries):
        try:
            id = '{0}-{1}'.format(prefix, random.randint(minID, maxID))
            data['_id'] = id
            return db.save(data)
        except couchdb.ResourceConflict:
            pass

    raise RuntimeError('Could not find unique id for new document.')


########################################
# jobs
########################################
def getJob(db, id):
    try:
        _id = 'job-{0}'.format(id)
        job = db[_id]
        del job['_id']
        del job['_rev']
        del job['type']

        return dict(job)
    except couchdb.ResourceNotFound:
        raise KeyError('Job {0} does not exist.'.format(id))


def addJob(db, id):
    # XXX add 'user' field here to prevent other users from waiting on foreign jobs?
    data = {}
    data['type'] = 'job'
    data['timestamp'] = int(time.time())
    data['state'] = 'pending'
    data['message'] = ''

    # do NOT use addDocumentUsingPrefixPlusUniqueID here!
    # we already got a unique identifier for the job from the outside
    _id = 'job-{0}'.format(id)
    db[_id] = data

    return id


def setJobState(db, id, state, message=''):
    try:
        _id = 'job-{0}'.format(id)
        job = db[_id]
        job['state'] = state
        job['message'] = message
        db[_id] = job
    except couchdb.ResourceNotFound:
        raise KeyError('Job {0} does not exist.'.format(id))
    except couchdb.ResourceConflict:
        # TODO should catch this and retry
        raise RuntimeError('CouchDB resource conflict on job "{0}".'.format(id))


def deleteJob(db, id):
    try:
        _id = 'job-{0}'.format(id)
        del db[_id]
    except couchdb.ResourceNotFound:
        raise KeyError('Job {0} does not exist.'.format(id))


def deleteOldJobs(db, olderThan=43200):
    now = time.time()

    for x in db.view('veerezo/jobs_by_state', include_docs='true'):
        if now - x.doc['timestamp'] > olderThan:
            try:
                del db[x.id]
            except couchdb.ResourceNotFound:
                pass


########################################
# images
########################################
def getImageIDs(db):
    imageIDs = []
    for x in db.view('veerezo/images_by_id'):
        id = int(x.key.split('-')[-1])
        imageIDs.append(id)

    return imageIDs


def getImage(db, id):
    try:
        _id = 'image-{0}'.format(id)
        image = db[_id]
        del image['_id']
        del image['_rev']
        del image['type']

        return dict(image)
    except couchdb.ResourceNotFound:
        raise KeyError('Image {0} does not exist.'.format(id))


########################################
# ssh keys
########################################
def getSSHKeyIDs(db, user=None):
    if user is None:
        viewParams = {}
    else:
        viewParams = {'startkey': user, 'endkey': user}

    IDs = []
    for x in db.view('veerezo/sshkeys_by_user', **viewParams):
        id = int(x.id.split('-')[-1])
        IDs.append(id)

    return IDs


def getSSHKey(db, id):
    try:
        _id = 'sshkey-{0}'.format(id)

        sshkey = db[_id]
        del sshkey['_id']
        del sshkey['_rev']
        del sshkey['type']

        return dict(sshkey)
    except couchdb.ResourceNotFound:
        raise KeyError('SSH key {0} does not exist.'.format(id))


def addSSHKey(db, key, user=None):
    data = {}
    data['type'] = 'sshkey'
    data['key'] = key
    data['user'] = user

    docID, rev = addDocumentUsingPrefixPlusUniqueID(db, 'sshkey', data)

    return int(docID.split('-')[-1])


def deleteSSHKey(db, id):
    try:
        _id = 'sshkey-{0}'.format(id)
        del db[_id]
    except couchdb.ResourceNotFound:
        raise KeyError('SSH key {0} does not exist.'.format(id))


########################################
# networks
########################################

def addNetwork(db, devices, user=None, tags=None):
    if tags is None:
        tags = []

    data = {}
    data['type'] = 'network'
    data['devices'] = []
    data['user'] = user
    data['tags'] = tags
    for x in devices:
        d = {'device': x}
        data['devices'].append(d)

    docID, rev = addDocumentUsingPrefixPlusUniqueID(db, 'network', data)

    return int(docID.split('-')[-1])


def deleteNetwork(db, id):
    try:
        _id = 'network-{0}'.format(id)
        del db[_id]
    except couchdb.ResourceNotFound:
        raise KeyError('Network {0} does not exist.'.format(id))


def getNetworkIDs(db, user=None, tags=None):
    if user is None:
        viewParams = {}
    else:
        viewParams = {'startkey': user, 'endkey': user}

    if tags is not None:
        tags = set(tags)

    networkIDs = []
    for x in db.view('veerezo/networks_by_user', **viewParams):
        if tags is not None:
            xTags = set(x.value)
            if not tags.issubset(xTags):
                continue

        id = int(x.id.split('-')[-1])
        networkIDs.append(id)

    return networkIDs


def getNetwork(db, id):
    _id = 'network-{0}'.format(id)

    try:
        network = db[_id]
        del network['_id']
        del network['_rev']
        del network['type']

        return dict(network)
    except couchdb.ResourceNotFound:
        raise KeyError('Could not find network {0}.'.format(id))


def getNetworkConfiguration(db):
    # TODO rewrite network.configureNetworks to directly use the format described here
    networkConfig = {}

    for x in db.view('veerezo/networks_by_id', include_docs='true'):
        networkID = int(x.key.split('-')[-1])

        devices = x.doc.get('devices', [])
        deviceList = [x['device'] for x in devices]

        networkConfig[networkID] = deviceList

    return networkConfig


########################################
# VMs
########################################
def addVM(db, imageRef, ramMiB, networkCards, networkConfig, user=None, tags=None):
    '''
    ramMiB                  amount of memory for the VM
    networkCards            list of [networkID, networkID, None, networkID] for the NICs 0, 1, 2, ...
    networkConfig           use VMNetworkConfigMaker to create this value
    '''
    if tags is None:
        tags = []

    vmConfig = {}
    vmConfig['user'] = user
    vmConfig['tags'] = tags
    vmConfig['type'] = 'vm'
    vmConfig['disks'] = [
        {'type': 'root', 'imageRef': imageRef},
        {'type': 'swap', 'sizeMiB': 256},
        {'type': 'data', 'sizeMiB': 128},
    ]
    vmConfig['emulation'] = {
        'ramMiB': ramMiB,
        'diskModel': 'ide',
        'nicModel': 'e1000',
    }

    vmConfig['networkCards'] = []
    for x in networkCards:
        if x is None:
            ref = None
        else:
            ref = 'network-{0}'.format(x)
        vmConfig['networkCards'].append({'networkRef': ref})

    vmConfig['networkConfiguration'] = networkConfig

    _id, _rev = addDocumentUsingPrefixPlusUniqueID(db, 'vm', vmConfig)
    id = int(_id.split('-')[-1])

    stateID = 'vmstate-{0}'.format(id)
    db[stateID] = {'type': 'vmstate', 'state': 'pending', 'user': user}

    return id



def deleteVM(db, id):
    try:
        _id = 'vmstate-{0}'.format(id)
        del db[_id]
    except couchdb.ResourceNotFound:
        pass # this may fail, just trying to keep the DB clean

    try:
        _id = 'vm-{0}'.format(id)
        del db[_id]
    except couchdb.ResourceNotFound:
        raise KeyError('VM {0} does not exist.'.format(id))


def getVMIDs(db, user=None, tags=None):
    if user is None:
        viewParams = {}
    else:
        viewParams = {'startkey': user, 'endkey': user}

    if tags is not None:
        tags = set(tags)

    vmIDs = []
    for x in db.view('veerezo/vms_by_user', **viewParams):
        if tags is not None:
            xTags = set(x.value)
            if not tags.issubset(xTags):
                continue

        id = int(x.id.split('-')[-1])
        vmIDs.append(id)
    return vmIDs


def getVMState(db, id):
    try:
        _id = 'vmstate-{0}'.format(id)
        vmState = db[_id]
        del vmState['_id']
        del vmState['_rev']
        del vmState['type']

        return dict(vmState)
    except couchdb.ResourceNotFound:
        raise KeyError('Could not find vm state "{0}".'.format(id))


def setVMState(db, id, newState):
    try:
        _id = 'vmstate-{0}'.format(id)

        vmState = db[_id]
        vmState['state'] = newState
        db[_id] = vmState # XXX this does not handle conflicts on this resource. does this matter?
    except couchdb.ResourceNotFound:
        raise KeyError('Could not find vm state "{0}".'.format(id))


def getVMIDsInState(db, state):
    viewParams = {'startkey': state, 'endkey': state}

    IDs = []
    for x in db.view('veerezo/vmstates_by_state', **viewParams):
        id = int(x.id.split('-')[-1])
        IDs.append(id)

    return IDs


def getVM(db, id):
    try:
        _id = 'vm-{0}'.format(id)
        vm = db[_id]
        del vm['_id']
        del vm['_rev']
        del vm['type']

        return dict(vm)
    except couchdb.ResourceNotFound:
        raise KeyError('Could not find vm "{0}".'.format(id))


def updateVM(db, id, updateData):
    try:
        _id = 'vm-{0}'.format(id)

        vm = db[_id]
        vm.update(updateData)
        db[_id] = vm
    except couchdb.ResourceNotFound:
        raise KeyError('Could not find vm "{0}".'.format(id))
    except couchdb.ResourceConflict:
        # TODO should catch this and retry
        raise RuntimeError('CouchDB resource conflict on vm "{0}".'.format(id))


def getVMConfiguration(db, id):
    vmConfig = db['vm-{0}'.format(id)]
    vmConfig = dict(vmConfig)

    user = vmConfig['user']
    del vmConfig['_id']
    del vmConfig['_rev']
    del vmConfig['type']
    del vmConfig['user']
    del vmConfig['tags']


    disks = vmConfig['disks']
    for x in disks:
        if x['type'] == 'root':
            imageRef = x['imageRef']
            del x['imageRef']
            image = db[imageRef]
            x['image'] = image['name']
            x['sizeMiB'] = image['sizeMiB']
            x['injector'] = image['injector']


    networkCards = vmConfig['networkCards']
    for x in networkCards:
        networkRef = x['networkRef']
        del x['networkRef']
        if networkRef:
            networkID = int(networkRef.split('-')[-1])
        else:
            networkID = None
        x['network'] = networkID


    authorizedKeys = []
    for x in getSSHKeyIDs(db, user):
        _id = 'sshkey-{0}'.format(x)

        key = db[_id]['key']
        authorizedKeys.append(key)
    vmConfig['authorizedKeys'] = authorizedKeys


    return vmConfig
