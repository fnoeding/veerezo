#!/usr/bin/env python

from distutils.core import setup

import sys
sys.path.insert(0, 'src')
import veerezo


setup(name='veerezo',
      version=veerezo.__version__,
      author='Florian Noeding',
      author_email='florian@noeding.com',
      url='https://bitbucket.org/fnoeding/veerezo',
      license='BSD',
      description='Virtual machine manager with flexible layer 2 network topologies.',
      long_description=open('README.md').read(),
      packages=['', 'veerezo', 'veerezo.backend', 'veerezo.frontend', 'veerezo.common'],
      package_dir={'veerezo': 'src/veerezo'},
      scripts=['src/bin/veerezo-rest-frontend', 'src/bin/veerezo-backend-worker'],
      package_data={'veerezo.backend': ['injectplugins/*.py']},
)
