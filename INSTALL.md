
Manual Installation
===================

This document is still work in progress and subject to change. If something is missing please submit a bug.


Dependencies
------------

Veerezo is mainly tested on Ubuntu 11.10 and there you can install the dependencies using the command:

        apt-get install beanstalkd bridge-utils couchdb e2fsprogs iproute kpartx kvm lvm2 openssh-client parted python python-couchdb python-paramiko python-requests python-webpy python-yaml udev util-linux

Additional dependencies, that are not available as Ubuntu 11.10 packages:

* python validictory: http://pypi.python.org/pypi/validictory



LVM setup
---------

Create a volume group called "veerezo". Read more about this in the [LVM-HOWTO](http://tldp.org/HOWTO/LVM-HOWTO/). If you just want to do some quick experiments you can create this volume group on a loopback device instead of repartitioning your harddisk.



Beanstalkd setup
----------------

Edit /etc/default/beanstalkd and uncomment the line "START=yes" and start the service:

        service beanstalkd start



CouchDB setup
-------------

Create a database called "veerezo". Read more about CouchDB in the [CouchDB Guide](http://guide.couchdb.org/). Here is the code to do this using Python:

        import couchdb
        couch = couchdb.Server()
        couch.create('veerezo')



System images and networks
--------------------------

You can download system images from https://bitbucket.org/fnoeding/veerezo/downloads instead of building your own images. For example install the Ubuntu 11.10 server system image:

        wget https://bitbucket.org/fnoeding/veerezo/downloads/ubuntu_11_10.raw.bz2
        lvcreate --name ubuntu_11_10 --size 4096MB veerezo
        cat ubuntu_11_10.raw.bz2 | bunzip2 | dd of=/dev/veerezo/ubuntu_11_10

Create a document describing the disk image in the "veerezo" database:

        import couchdb
        couch = couchdb.Server()
        db = couch['veerezo']
        db['image-1'] = {
            'type': 'image',
            'name': 'ubuntu_11_10',
            'sizeMiB': 4096,
            'injector': 'debian'
        }

**NOTE** the document ID for images must match the regex "image-\d+". This is also true for other types like "network", "vm", or "sshkey". This non-ideomatic use of CouchDB simplifies the creation of globally unique identifiers for Veerezo's internal usage.

Now the above disk image can be used by veerezo.

Also create an initial network to be used by the virtual machines:

        db['network-1'] = {
            'type': 'network',
            'devices': [],
            'user': null,
            'userTags': []
        }

The network with the id "network-1" has at the moment a special function: It is the only network that is available to all users (even though it's not returned in network lists). It can be used to give VMs access to the outside world. After you started the backend worker process (see below) you can assign the bridge device "rezobr1" an IP address or bridge it with another ethernet device.


Daemons
-------

Start the processes (use two different shells - these processes do not daemonize themselves):

        sudo python veerezo-backend-worker
        python veerezo-rest-frontend

if you want to start veerezo without installing it into your system you have to set the PYTHONPATH:

        cd src/bin
        sudo PYTHONPATH=~/veerezo/src veerezo-backend-worker
        PYTHONPATH=~/veerezo/src python veerezo-rest-frontend

assuming you have checked out a clone into ~/veerezo.

Each time the veerezo-backend-worker is started the CouchDB design for the veerezo database is updated.


Test Veerezo
------------

When you followed all of the above steps you should now be able to create virtual machines. But first check if there's a bridge device called "rezobr1" and give it an IP address:

        # look for bridge devices
        # this should display at least "rezobr1"
        sudo brctl show

        # give it an IPv4 address
        sudo ip addr add 192.168.42.1/24 dev rezobr1

Upload a ssh key (this needs to be done only once):

        import pyveerezo
        import os

        veerezo = pyveerezo.VeerezoClient('http://veerezo-server.example:8080', 'username', 'password')

        with open(os.path.expanduser('~/.ssh/id_dsa.pub')) as f: # path may differ. If you do not have a ssh key create one using ssh-keygen
            key = f.read()
            veerezo.addSSHKey(key)

Now run the script clients/pyveerezo/examples/simple.py. This script will create a virtual machine with the IP address 192.168.42.100/24 and the default gateway 192.168.42.1. When the VM is running try to ping it:

        ping -c 5 192.168.42.100

It might take some time until the VM is available, so try it again if it fails. You should now be able to ssh into the virtual machine using

        ssh root@192.168.42.100

ssh will complain about not knowing the hosts fingerprint, but that is ok here. If it tells you the host key changed, use

        ssh-keygen -R 192.168.42.100

to remove the old host key entry. Important: Ignoring these warnings is only ever ok, if you are carefully checking that you are really talking to the right computer.

After logging into the virtual machine try to access the gateway using ping. You will not be able to reach the Internet without enabling IP forwarding on the host system:

        # (needs to be done after each reboot, or edit /etc/sysctl.conf)
        echo 1 > /proc/sys/net/ipv4/ip_forward

        # and add a NAT (network address translation) rule to netfilter
        # $DEVICE_TO_INTERNET is probably eth0 or ppp0
        iptables -t nat -A POSTROUTING -o $DEVICE_TO_INTERNET -j MASQUERADE

Instead of using NAT you can also add another ethernet device to rezobr1 by editing the "network-1" document in CouchDB, field "devices":

        {
            ...,
            "devices": [
                {"device": "eth0"}
            ],
            ...
        }
and restarting the backend worker process.
